import React, { useCallback, useEffect, useState } from 'react'
import { Message } from 'typings'
import * as S from './styles'
import { uniqueId } from 'lodash'

interface MessagesProps {
  messages: Message[]
  identity: string
}

export const Messages = ({ messages, identity }: MessagesProps) => {
  const [sequences, setSequences] = useState<Message[][]>([])

  const createSequences = useCallback(() => {
    // Sequence is a row of messages sent by the same user
    let sequence: Message[] = []

    /**
     * Separate each sequence of messages and add to sequences array
     * We do that so it will be easier to render respective user
     * message with its own avatar and name
     */
    const newSequences: Message[][] = messages.reduce((acc, curr, i) => {
      const after = messages[i + 1]
      sequence.push(curr)

      // If theres no message after or the next message author is
      // differente, creates a new sequence
      if (after === undefined || curr.author !== after.author) {
        acc.push(sequence)
        sequence = []
      }

      return acc
    }, [] as Message[][])

    setSequences(newSequences)
  }, [messages])

  const renderSequences = () => {
    return sequences.map((sequence) => {
      const { author } = sequence[0]
      const sender = author === identity

      return (
        <S.SequencesWrapper key={uniqueId()} sender={sender}>
          <S.AvatarWrapper sender={sender}>
            <S.Avatar
              src='https://via.placeholder.com/10'
              alt='Avatar'
              sender={sender}
            />
          </S.AvatarWrapper>

          <S.Sequences>
            {!sender && <S.Username>{author}</S.Username>}
            <S.BubblesWrapper>
              {renderSequenceOfMessages(sequence, sender)}
            </S.BubblesWrapper>
          </S.Sequences>
        </S.SequencesWrapper>
      )
    })
  }

  const renderSequenceOfMessages = (sequence: Message[], sender: boolean) => {
    let arrowPosition: 'top' | 'middle' | 'bottom'

    return sequence.map((message, i) => {
      if (i === 0) arrowPosition = 'bottom'
      else if (i === sequence.length - 1) arrowPosition = 'top'
      else arrowPosition = 'middle'

      return (
        <S.Balloon
          key={uniqueId()}
          arrowPosition={arrowPosition}
          sender={sender}
        >
          {message.body}
        </S.Balloon>
      )
    })
  }

  useEffect(() => {
    createSequences()
  }, [createSequences])

  return <S.Messages>{renderSequences()}</S.Messages>
}
