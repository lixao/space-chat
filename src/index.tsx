import React, { FormEvent, useEffect, useRef, useState } from 'react'
import chatProviderFactory from './providers'
import { ChatProps, Message, Paginate } from './typings'
import InfiniteScroll from 'react-infinite-scroller'
import { GlobalStyles, Theme } from 'utils/styles'
import { ThemeProvider } from 'styled-components'
import { Form, Messages } from 'components'

export const Chat = ({
  provider,
  token,
  identity,
  channelSid,
  channelName,
  inputProps,
  /** Functions */
  onChannels,
  onReady,
  onError,
  onTokenExpired
}: ChatProps) => {
  const inputRef = useRef<HTMLInputElement>(null)
  const messagesContainerRef = useRef<HTMLDivElement>(null)
  const chatProvider = useRef(chatProviderFactory(provider, token))
  const lastMessages = useRef<Paginate<Message>>()

  const [messages, setMessages] = useState<Array<Message>>([])
  const [loading, setLoading] = useState(false)
  const [loadingMore, setLoadingMore] = useState(false)

  async function init() {
    setLoading(true)

    try {
      await chatProvider.current.start({
        channelSid,
        channelName
      })

      if (onChannels) {
        onChannels(await chatProvider.current.getChannels())
      }

      if (channelSid || channelName) {
        lastMessages.current = await chatProvider.current.getMessages()

        setMessages(lastMessages.current.items)
      }

      /** Adding listeners */
      chatProvider.current.addMessageEventListener(handleMessageAdded)
      if (onTokenExpired) {
        chatProvider.current.addTokenExpiredEventListener(onTokenExpired)
      }

      /** Starting all chat event listeners */
      chatProvider.current.startListening()

      scrollToBottom()
    } catch (error) {
      if (onError) {
        onError(error)
      }
    }

    // Wait until scroll to bottom
    setTimeout(() => {
      setLoading(false)

      if (onReady) {
        onReady()
      }
    }, 2000)
  }

  function handleSendMessage(event: FormEvent<HTMLFormElement>) {
    event.preventDefault()

    if (!inputRef.current) return

    const message = inputRef.current.value

    if (message) {
      inputRef.current.value = ''
      inputRef.current.focus()
      try {
        chatProvider.current.sendMessage(message)
      } catch (error) {
        if (onError) {
          onError(error)
        }
      }
    }
  }

  function handleMessageAdded(message: Message) {
    setMessages((prevState) => [...prevState, message])

    scrollToBottom()
  }

  function scrollToBottom() {
    if (
      messagesContainerRef.current &&
      messagesContainerRef.current.lastElementChild?.lastElementChild
    ) {
      messagesContainerRef.current.lastElementChild.lastElementChild.scrollIntoView(
        {
          behavior: 'smooth'
        }
      )
    }
  }

  async function loadMore() {
    if (lastMessages.current && lastMessages.current.prevPage) {
      setLoadingMore(true)
      lastMessages.current = await lastMessages.current.prevPage()

      const newMessages = lastMessages.current.items || []
      setMessages((prevState) => [...newMessages, ...prevState])
      setLoadingMore(false)
    }
  }

  useEffect(() => {
    if (token) {
      init()
    }

    return () => {
      /** Stopping all chat event listeners */
      chatProvider.current.stopListening()
    }
  }, [token, channelSid, channelName])

  return (
    <ThemeProvider theme={Theme}>
      <GlobalStyles />

      <div
        ref={messagesContainerRef}
        style={{ height: '100%', overflow: 'auto', width: '100%' }}
      >
        {loadingMore && <div>Loading...</div>}

        <InfiniteScroll
          pageStart={0}
          useWindow={false}
          initialLoad={false}
          loadMore={loadMore}
          hasMore={
            lastMessages.current?.hasPrevPage && !loading && !loadingMore
          }
          getScrollParent={() => messagesContainerRef.current}
          isReverse
        >
          <Messages messages={messages} identity={identity} />
        </InfiniteScroll>
      </div>

      {loading && <div>Connecting...</div>}

      <Form
        onSubmit={handleSendMessage}
        ref={inputRef}
        disabled={inputProps?.disabled || loading}
      />
    </ThemeProvider>
  )
}
