import React, { FormEvent, forwardRef } from 'react'
import * as S from './styles'
import { ReactComponent as Send } from './send.svg'

export interface FormProps {
  onSubmit: (event: FormEvent<HTMLFormElement>) => void
  disabled: boolean
}

export const Form = forwardRef<HTMLInputElement, FormProps>(
  ({ disabled, onSubmit }, ref) => (
    <S.Form onSubmit={onSubmit}>
      <S.Input
        ref={ref}
        placeholder='Type tour message here'
        disabled={disabled}
      />

      <S.ButtonWrapper>
        <S.Button type='submit' disabled={disabled}>
          <Send height={30} />
        </S.Button>
      </S.ButtonWrapper>
    </S.Form>
  )
)

Form.displayName = 'Form'
