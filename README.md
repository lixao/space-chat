# space-chat

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/space-chat.svg)](https://www.npmjs.com/package/space-chat) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save space-chat
```

or

```bash
yarn add space-chat
```

## Usage

### Creating or finding a channel by unique name

```tsx
import { Chat } from 'space-chat'

function App() {
  return (
    <div>
      <Chat
        provider='twilio'
        token='user-token'
        identity='jon-doe'
        channelName='unique-channel-name'
        onReady={() => console.log('Chat is ready')}
        onError={(error) => console.log('Chat error', error)}
      />
    </div>
  )
}

export default App
```

### Finding a channel by Sid

```tsx
import { Chat } from 'space-chat'

function App() {
  return (
    <div>
      <Chat
        provider='twilio'
        token='user-token'
        identity='jon-doe'
        channelSid='CH164f325e19364483a3d5531581afb990'
        onReady={() => console.log('Chat is ready')}
        onError={(error) => console.log('Chat error', error)}
      />
    </div>
  )
}

export default App
```

## License

MIT © [Joyjet](https://joyjet.com)
