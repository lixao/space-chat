export const Theme = {
  white: '#ffffff',
  primary: '#009fd4',
  grays: {
    300: '#61666c',
    200: '#c3c5c7',
    100: '#edeef2'
  }
}
