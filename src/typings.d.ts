/* eslint-disable no-unused-vars */
/**
 * Default CSS definition for typescript,
 * will be overridden with file-specific definitions by rollup
 */

import { Client as TwilioClient } from 'twilio-chat'
import { Channel as TwilioChannel } from 'twilio-chat/lib/channel'

declare module '*.css' {
  const content: { [className: string]: string }
  export default content
}

interface SvgrComponent
  extends React.StatelessComponent<React.SVGAttributes<SVGElement>> {}

declare module '*.svg' {
  const svgUrl: string
  const svgComponent: SvgrComponent
  export default svgUrl
  export { svgComponent as ReactComponent }
}

export type Providers = 'twilio'

export type ClientTypes = TwilioClient

export type ChannelTypes = TwilioChannel

export interface FindOrCreateChannel {
  channelSid?: string
  channelName?: string
}

export type StartChatOptions = FindOrCreateChannel

export interface Paginate<Item> {
  hasNextPage: boolean
  hasPrevPage: boolean
  items: Array<Item>
  nextPage?: () => Promise<Paginate<Item>>
  prevPage?: () => Promise<Paginate<Item>>
}

export interface Member {
  identity: string
  isTyping?: boolean
  online?: boolean
  createdAt: date
}

export interface Message {
  author: string
  body: string
  createdAt?: Date
}

export interface InputProps {
  disabled?: boolean
}

export interface ChatProps {
  identity: string
  channelName?: string
  channelSid?: string
  provider: Providers
  token: string
  inputProps?: InputProps
  /** Functions */
  onChannels?: (channels: Paginate<ChannelTypes>) => void
  onReady?: () => void
  onError?: (error: any) => void
  onTokenExpired?: () => void
}
