import 'styled-components'
import Theme from './utils/styles/theme' // Import type from above file

type ThemeType = typeof Theme

declare module 'styled-components' {
  export interface DefaultTheme extends ThemeType {} // extends the global DefaultTheme with our ThemeType.
}
