import React from 'react'
import { Meta, Story } from '@storybook/react/types-6-0'
import { Messages, MessagesProps } from '.'

export default {
  title: 'Messages',
  component: Messages,
  args: {
    identity: 'Mary',
    messages: [
      {
        author: 'Mary',
        body: 'Hello there!'
      },
      {
        author: 'Mary',
        body: 'Lorem ipsum dolor sit amet'
      },
      {
        author: 'Suse',
        body: 'All fine'
      },
      {
        author: 'Suse',
        body:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in volutpat diam, a ultrices libero. Etiam vitae aliquam lorem. Duis vitae neque dictum, luctus lectus eget, pretium enim. Nam et dui ac libero laoreet vehicula. In rhoncus leo quam, non euismod tortor porttitor vitae. Donec eget magna et turpis aliquet lacinia. Nunc eleifend feugiat tristique. Donec venenatis consectetur fringilla. In ac tellus tempor, semper risus non, efficitur arcu. Pellentesque scelerisque at odio sit amet finibus. Mauris tincidunt nisl non luctus hendrerit. Etiam in felis quis enim blandit luctus et et purus.'
      },
      {
        author: 'Suse',
        body: 'Nem cuptur sint voluptatet landus.'
      }
    ]
  }
} as Meta

export const Default: Story<MessagesProps> = (args) => <Messages {...args} />
