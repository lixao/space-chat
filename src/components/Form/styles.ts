import styled from 'styled-components'

export const Form = styled.form`
  width: 100%;
  border-top: 1px solid ${({ theme }) => theme.grays[100]};
  padding: 2rem 5rem;
  display: flex;
  justify-content: stretch;
  align-items: center;

  > *:focus-within {
    outline: none;
    box-shadow: 0 0 0.5rem 0 ${({ theme }) => theme.primary};
  }
`

export const Input = styled.input`
  background-color: ${({ theme }) => theme.grays[100]};
  border: none;
  padding: 1rem 1.8rem;
  border-radius: 5rem;
  width: 95%;
`

export const ButtonWrapper = styled.div`
  width: 5%;
  display: flex;
  align-items: center;
  justify-content: center;
`

export const Button = styled.button`
  border: none;
  background: transparent;
  cursor: pointer;

  svg {
    transition: fill 0.3s ease-in;
    fill: ${({ theme }) => theme.primary};
  }

  :focus {
    outline: 0;
  }

  :disabled {
    svg {
      fill: ${({ theme }) => theme.grays[100]};
    }
  }
`
