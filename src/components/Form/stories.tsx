import React, { useRef } from 'react'
import { Meta, Story } from '@storybook/react/types-6-0'
import { action } from '@storybook/addon-actions'
import { Form, FormProps } from '.'

const inputRef = useRef<HTMLInputElement>(null)

export default {
  title: 'Form',
  component: Form,
  args: {
    onSubmit: () => action('Send'),
    disabled: false,
    ref: inputRef
  }
} as Meta

export const Default: Story<FormProps> = (args) => <Form {...args} />
