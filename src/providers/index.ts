import { ChannelTypes, ClientTypes, Providers } from '../typings'
import ChatProvider from './chatProvider'
import { Twilio } from './twilio'

export default function chatProviderFactory(
  provider: Providers,
  token: string
): ChatProvider<ClientTypes, ChannelTypes> {
  switch (provider) {
    case 'twilio':
      return new Twilio(token)
    default:
      throw new Error('Provider not found')
  }
}
