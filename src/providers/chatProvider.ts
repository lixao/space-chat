import {
  FindOrCreateChannel,
  Member,
  Message,
  Paginate,
  StartChatOptions
} from '../typings'

export default abstract class ChatProvider<Client, Channel> {
  constructor(protected token: string) {}

  abstract start(options?: StartChatOptions): Promise<void>

  abstract sendMessage(message: string): Promise<void>
  abstract getChannels(): Promise<Paginate<Channel>>
  abstract getMessages(): Promise<Paginate<Message>>
  abstract getMembers(): Promise<Array<Member>>
  abstract startListening(): void
  abstract stopListening(): void

  abstract createClient(token?: string): Promise<Client>
  abstract findOrCreateChannel(data: FindOrCreateChannel): Promise<Channel>
  abstract joinChannel(channel?: Channel): Promise<Channel | undefined>

  /** Event Listeners */
  abstract addMessageEventListener(callback: (message: Message) => void): void
  abstract addTokenExpiredEventListener(callback: () => void): void
}
