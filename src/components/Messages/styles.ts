import styled, { css } from 'styled-components'

interface Sender {
  sender: boolean
}

interface BallonProps extends Sender {
  arrowPosition: 'top' | 'middle' | 'bottom'
}

export const Balloon = styled.div<BallonProps>`
  border-radius: 2rem;
  padding: 1rem 2rem;
  font-size: 1.6rem;
  line-height: 1.5;

  /* Styles based on sender */
  ${({ sender, theme }) =>
    sender
      ? css`
          align-self: flex-end;
          background-color: ${theme.primary};
          color: ${theme.white};
        `
      : css`
          align-self: flex-start;
          background-color: ${theme.grays[100]};
          color: ${theme.grays[300]};
        `}

  /* Position bubble arrow */
  ${({ arrowPosition, sender }) => {
    const side = sender ? 'right' : 'left'
    const styles = {
      top: css`
        border-top-${side}-radius: 0;
        margin-top: 1rem;
      `,
      middle: css`
        border-top-${side}-radius: 0;
        border-bottom-${side}-radius: 0;
        margin-top: 1rem;
      `,
      bottom: css`
        border-bottom-${side}-radius: 0;
      `
    }

    return styles[arrowPosition]
  }}
`

export const Messages = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  padding: 2rem;
`

export const SequencesWrapper = styled.div<Sender>`
  display: flex;
  flex-direction: ${({ sender }) => (sender ? 'row-reverse' : 'row')};
`

export const Sequences = styled.div`
  display: flex;
  flex-wrap: wrap;
  max-width: 50%;
`

export const BubblesWrapper = styled.div`
  display: flex;
  flex-direction: column;
`

export const Username = styled.span`
  text-transform: uppercase;
  font-size: 1.2rem;
  font-weight: bold;
  color: ${({ theme }) => theme.grays[300]};
  margin-bottom: 0.5rem;
  width: 100%;
`

export const Avatar = styled.img<Sender>`
  border-radius: 50%;

  ${({ sender }) =>
    sender
      ? css`
          width: 2rem;
          height: 2rem;
        `
      : css`
          width: 2.8rem;
          height: 2.8rem;
        `}
`

export const AvatarWrapper = styled.div<Sender>`
  display: flex;
  margin-top: 2rem;

  ${({ sender }) =>
    sender
      ? css`
          margin-left: 1rem;
          align-items: flex-end;
        `
      : css`
          margin-right: 1rem;
        `}
`
