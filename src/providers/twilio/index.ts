import ChatProvider from '../chatProvider'
import { Client } from 'twilio-chat'
import { Channel } from 'twilio-chat/lib/channel'
import { Message as TwilioMessage } from 'twilio-chat/lib/message'
import {
  FindOrCreateChannel,
  Member,
  Message,
  StartChatOptions
} from '../../typings'

export class Twilio extends ChatProvider<Client, Channel> {
  constructor(
    protected token: string,
    private _client?: Client,
    private _channel?: Channel,
    private _messageEventListeners?: Array<(message: Message) => void>,
    private _tokenExpiredEventListeners?: Array<() => void>
  ) {
    super(token)
  }

  _handleMessageAdded(
    message: TwilioMessage,
    messageEventListeners?: Array<(message: Message) => void>
  ) {
    if (!messageEventListeners) return

    messageEventListeners.forEach((mel) =>
      mel({
        author: message.author,
        body: message.body,
        createdAt: message.dateCreated
      })
    )
  }

  _handleTokenExpired() {
    if (!this._tokenExpiredEventListeners) return

    this._tokenExpiredEventListeners.forEach((callback) => callback())
  }

  async start(options?: StartChatOptions) {
    if (!this.token) {
      throw new Error('Token not provided')
    }

    await this.createClient(this.token)

    if (options?.channelName || options?.channelSid) {
      await this.findOrCreateChannel({
        channelSid: options.channelSid,
        channelName: options.channelName
      })

      await this.joinChannel()
    }
  }

  async createClient(token: string) {
    this._client = await Client.create(token)

    return this._client
  }

  async findOrCreateChannel({ channelSid, channelName }: FindOrCreateChannel) {
    if (!this._client) {
      throw new Error('Client is undefined')
    }

    if (!channelSid && !channelName) {
      throw new Error('Both channelSid or channelName can not be undefined')
    }

    try {
      if (channelSid) {
        this._channel = await this._client.getChannelBySid(channelSid)
      } else {
        this._channel = await this._client.getChannelByUniqueName(
          channelName || ''
        )
      }

      return this._channel
    } catch (error) {
      if (channelName) {
        this._channel = await this._client.createChannel({
          uniqueName: channelName,
          friendlyName: channelName
        })

        return this._channel
      }

      throw new Error('Channel not found')
    }
  }

  async joinChannel(channel?: Channel) {
    const auxChannel = channel || this._channel

    if (!auxChannel) {
      throw new Error('Channel is undefined')
    }

    if (auxChannel.status !== 'joined') {
      this._channel = await auxChannel.join()
    }

    return this._channel
  }

  async sendMessage(message: string) {
    if (!this._channel) {
      throw new Error('Channel is undefined')
    }

    this._channel.sendMessage(message)
  }

  async getChannels() {
    if (!this._client) {
      throw new Error('Client is undefined')
    }

    const paginatedChannels = await this._client.getSubscribedChannels()

    return paginatedChannels
  }

  async getMessages() {
    if (!this._channel) {
      throw new Error('Channel is undefined')
    }

    const paginatedMessages = await this._channel.getMessages()

    const messages: Array<Message> =
      paginatedMessages.items.map((message) => ({
        author: message.author,
        body: message.body,
        createdAt: message.dateCreated
      })) || []

    return {
      hasNextPage: paginatedMessages.hasNextPage,
      hasPrevPage: paginatedMessages.hasPrevPage,
      items: messages,
      nextPage: paginatedMessages.nextPage,
      prevPage: paginatedMessages.prevPage
    }
  }

  async getMembers() {
    if (!this._channel) {
      throw new Error('Channel is undefined')
    }

    const members = await this._channel.getMembers()

    const participants: Array<Member> = members.map((member) => ({
      identity: member.identity,
      isTyping: member.isTyping,
      createdAt: member.dateCreated
    }))

    return participants
  }

  startListening() {
    if (this._channel) {
      const that = this

      this._channel.addListener('messageAdded', (message) =>
        that._handleMessageAdded(message, that._messageEventListeners)
      )
    }

    if (this._client) {
      this._client.on('tokenAboutToExpire', this._handleTokenExpired)

      this._client.on('tokenExpired', this._handleTokenExpired)
    }
  }

  stopListening() {
    if (this._channel) {
      this._channel.removeAllListeners('messageAdded')
    }

    if (this._client) {
      this._client.removeAllListeners('tokenAboutToExpire')
      this._client.removeAllListeners('tokenExpired')
    }
  }

  async addMessageEventListener(callback: (message: Message) => void) {
    if (this._messageEventListeners === undefined) {
      this._messageEventListeners = []
    }

    this._messageEventListeners.push(callback)
  }

  async addTokenExpiredEventListener(callback: () => void) {
    if (this._tokenExpiredEventListeners === undefined) {
      this._tokenExpiredEventListeners = []
    }

    this._tokenExpiredEventListeners.push(callback)
  }
}
